# BI Group UI-Design

## Установка пакетов

Надо запустить

```bash
yarn install
```

## Запуск

Запускаем

```bash
yarn storybook
```

## Публикация

Запускаем, авторизуемся в npm

```bash
npm run build
```

## ссылка на пакет npm

https://www.npmjs.com/package/big-design-ui