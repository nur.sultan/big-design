FROM nginx:1.14.0

COPY ./storybook-static/ /usr/share/nginx/html/

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]