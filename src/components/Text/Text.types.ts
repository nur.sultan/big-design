import { HTMLAttributes } from 'react';

export interface Props extends HTMLAttributes<HTMLElement> {
  textStyle?: TextStyle;
  component?: 'span' | 'div' | 'p' | 'h2';
}

export enum TextVariants {
  H72 = 'h72',
  H56 = 'h56',
  H40 = 'h40',
  H32 = 'h32',
  H24 = 'h24',
  H20 = 'h20',
  H16 = 'h16',
  H14 = 'h14',
  H12 = 'h12',
  P24 = 'p24',
  P20 = 'p20',
  P16 = 'p16',
  P14 = 'p14',
  P12 = 'p12',
}

export type TextStyle = typeof TextVariants[keyof typeof TextVariants];
