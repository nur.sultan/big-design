import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import FlexBox from '../FlexBox';
import Text from './Text';
import { TextVariants } from './Text.types';

export default {
  title: 'Design System/Text',
  component: Text,
  argTypes: {},
  // eslint-disable-next-line prettier/prettier
} as ComponentMeta<typeof Text>;

const BaseTemplate: ComponentStory<typeof Text> = (args) => <Text {...args}>Default Text</Text>;

export const Base = BaseTemplate.bind({});
Base.args = {};

const VariantsTemplate: ComponentStory<typeof Text> = () => (
  <FlexBox flexDirection={'column'}>
    <Text
      component="h2"
      textStyle={TextVariants.H72}
    >
      Headings72
    </Text>
    <Text
      component="h2"
      textStyle={TextVariants.H56}
    >
      Headings56
    </Text>
    <Text
      component="h2"
      textStyle={TextVariants.H40}
    >
      Headings40
    </Text>
    <Text
      component="h2"
      textStyle={TextVariants.H32}
    >
      Headings32
    </Text>
    <Text
      component="h2"
      textStyle={TextVariants.H24}
    >
      Headings24
    </Text>
    <Text
      component="h2"
      textStyle={TextVariants.H20}
    >
      Headings20
    </Text>
    <Text
      component="h2"
      textStyle={TextVariants.H16}
    >
      Headings16
    </Text>
    <Text
      component="h2"
      textStyle={TextVariants.H14}
    >
      Headings14
    </Text>
    <Text
      component="p"
      textStyle={TextVariants.P24}
    >
      Paragraph24
    </Text>
    <Text
      component="p"
      textStyle={TextVariants.P20}
    >
      Paragraph22
    </Text>
    <Text
      component="p"
      textStyle={TextVariants.P16}
    >
      Paragraph16
    </Text>
    <Text
      component="p"
      textStyle={TextVariants.P14}
    >
      Paragraph14
    </Text>
    <Text
      component="p"
      textStyle={TextVariants.P12}
    >
      Paragraph12
    </Text>
  </FlexBox>
);

export const Variants = VariantsTemplate.bind({});
Variants.args = {};
