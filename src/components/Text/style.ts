import styled, { css } from 'styled-components';
import { color, display, margin, size, variant } from 'styled-system';

import { TextVariants } from './Text.types';

const textStyles = variant({
  prop: 'textStyle',
  variants: {
    [TextVariants.H72]: {
      fontSize: 72,
      fontWeight: 600,
    },
    [TextVariants.H56]: {
      fontSize: 56,
      fontWeight: 600,
    },
    [TextVariants.H40]: {
      fontSize: 40,
      fontWeight: 600,
    },
    [TextVariants.H32]: {
      fontSize: 32,
      fontWeight: 600,
    },
    [TextVariants.H24]: {
      fontSize: 24,
      fontWeight: 600,
    },
    [TextVariants.H20]: {
      fontSize: 20,
      fontWeight: 600,
    },
    [TextVariants.H16]: {
      fontSize: 16,
      fontWeight: 600,
    },
    [TextVariants.H14]: {
      fontSize: 14,
      fontWeight: 600,
    },
    [TextVariants.H12]: {
      fontSize: 12,
      fontWeight: 600,
    },
    [TextVariants.P24]: {
      fontSize: 24,
      fontWeight: 600,
    },
    [TextVariants.P20]: {
      fontSize: 20,
      fontWeight: 400,
    },
    [TextVariants.P16]: {
      fontSize: 16,
      fontWeight: 400,
    },
    [TextVariants.P14]: {
      fontSize: 14,
      fontWeight: 400,
    },
    [TextVariants.P12]: {
      fontSize: 12,
      fontWeight: 400,
    },
  },
});

const textProps = css`
  margin: 0px 0px 0.35em;
  color: #000;

  ${textStyles}
  ${margin}
  ${display}
  ${size}
  ${color}
`;

const Text = styled.div`
  ${textProps}
`;

export const Span = styled.span`
  ${textProps}
`;

export const Paragraph = styled.p`
  ${textProps}
`;

export default Text;
