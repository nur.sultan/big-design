import React, { FC } from 'react';

import { default as Typography, Paragraph, Span } from './style';
import { Props } from './Text.types';

const getComponent = (component: string) => {
  switch (component) {
    case 'div':
      return Typography;
    case 'span':
      return Span;
    case 'p':
      return Paragraph;
    default:
      return Typography;
  }
};

const Text: FC<Props> = ({ children, component = 'div', ...rest }) => {
  const Template = getComponent(component);
  return <Template {...rest}>{children}</Template>;
};

export default Text;
