import { HTMLAttributes } from 'react';

export type SelectOption = {
  label: string;
  value: string | number | boolean | null;
  isdisabled?: boolean | null;
};

export interface Props extends HTMLAttributes<HTMLInputElement> {
  onChange?: () => void;
  disabled?: boolean;
  placeHolder?: string;
  options?: SelectOption[];
  label?: string;
}
