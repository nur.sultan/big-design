import styled from 'styled-components';
import { variant } from 'styled-system';

type ContainerProps = {
  disabled?: boolean;
};

export const Wrapper = styled.div<ContainerProps>`
  margin-left: 4px;

  .select-box {
    margin: 4px 0 4px 0;
  }

  label {
    font-weight: 400;
    font-size: 14px;
    margin-bottom: 4px;
    color: ${(props) => (props.disabled ? '#9E9E9E' : 'inherit')};
  }
`;

const helperVariants = variant({
  prop: 'color',
  variants: {
    error: {
      color: '#BA1B1B',
    },
    success: {
      color: '#25AA51',
    },
    disabled: {
      color: '#9E9E9E',
    },
  },
});

type HelperTextProps = {
  color?: 'error' | 'success' | 'disabled';
};

export const HelperText = styled.div<HelperTextProps>`
  font-weight: 400;
  font-size: 12px;
  color: inherit;

  ${helperVariants}
`;
