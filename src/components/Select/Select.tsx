import React, { FC } from 'react';
import { default as SelectBox } from 'react-select';

import { Props } from './Select.types';
import { HelperText, Wrapper } from './style';

const Select: FC<Props> = ({
  onChange,
  options = [],
  disabled = false,
  placeHolder = 'Выберите',
  label,
}) => (
  <Wrapper disabled={disabled}>
    <label>{label}</label>
    <SelectBox
      components={{
        IndicatorSeparator: null,
      }}
      styles={{
        container: (prevStyles) => ({
          ...prevStyles,
          fontSize: 14,
        }),
        input: (prevStyles) => ({
          ...prevStyles,
          padding: 0,
        }),
        control: (prevStyles, { isFocused }) => ({
          ...prevStyles,
          borderColor: isFocused ? '#10579F' : prevStyles.borderColor,
          boxShadow: isFocused ? '#D0E1F2' : prevStyles.boxShadow,
          minHeight: 32,
        }),
        option: (prevStyles, { isSelected, isFocused }) => ({
          ...prevStyles,
          '&:hover': {
            backgroundColor: isFocused ? '#F5F5F5' : '',
          },
          color: isSelected ? '#1369BF' : prevStyles.color,
          backgroundColor: isSelected ? '#EEF4FA' : 'transparent',
        }),
        dropdownIndicator: (prevStyles) => ({
          ...prevStyles,
          padding: 4,
        }),
      }}
      className="select-box"
      isDisabled={disabled}
      isOptionDisabled={(option) => !!option.isdisabled}
      onChange={onChange}
      options={options}
      placeholder={placeHolder}
    />
    <HelperText color={disabled ? 'disabled' : 'error'}>Input helper text</HelperText>
  </Wrapper>
);

export default Select;
