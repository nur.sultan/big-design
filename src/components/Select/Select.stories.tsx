import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import FlexBox from '../FlexBox';
import Select from './Select';

export default {
  title: 'Design System/Select',
  component: Select,
  argTypes: {},
  // eslint-disable-next-line prettier/prettier
} as ComponentMeta<typeof Select>;

const BaseTemplate: ComponentStory<typeof Select> = (args) => (
  <Select
    {...args}
    options={[
      { value: 'chocolate1', label: 'Chocolate1' },
      { value: 'strawberry1', label: 'Strawberry1' },
      { value: 'vanilla1', label: 'Vanilla1', isdisabled: true },
    ]}
    label={'Input Label'}
  />
);

export const Base = BaseTemplate.bind({});
Base.args = {};

const VariantsTemplate: ComponentStory<typeof Select> = (args) => (
  <FlexBox
    flexDirection={'column'}
    justifyItems={'center'}
  >
    <Select
      {...args}
      options={[
        { value: 'chocolate1', label: 'Chocolate1' },
        { value: 'strawberry1', label: 'Strawberry1' },
        { value: 'vanilla1', label: 'Vanilla1', isdisabled: true },
      ]}
      label={'Input Label'}
    />
    <br />
    <Select
      {...args}
      options={[
        { value: 'chocolate1', label: 'Chocolate1' },
        { value: 'strawberry1', label: 'Strawberry1' },
        { value: 'vanilla1', label: 'Vanilla1', isdisabled: true },
      ]}
      disabled={true}
      label={'Input Label Disabled'}
    />
  </FlexBox>
);

export const Variants = VariantsTemplate.bind({});
Variants.args = {};
