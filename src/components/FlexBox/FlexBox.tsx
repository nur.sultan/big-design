/* eslint-disable prettier/prettier */
import styled from "styled-components";
import {
  flexbox,
  FlexboxProps,
  layout,
  margin,
  MarginProps,
} from "styled-system";

const FlexBox = styled.div<FlexboxProps & MarginProps>`
  display: flex;

  ${layout}
  ${flexbox}
  ${margin}
`;

export default FlexBox;
