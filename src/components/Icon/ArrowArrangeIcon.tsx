import React from 'react';

import { IconProps } from './Icon.types';

const ArrowArrange: React.FC<Pick<IconProps, 'strokeColor'>> = () => {
  return (
    <svg
      fill="currentColor"
      height="16"
      width="16"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10.7353 7.19534L12.6667 5.26468L10.7353 3.33334"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M3.33334 5.26665H12.6667"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M5.26468 8.80466L3.33334 10.7353L5.26468 12.6667"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.6667 10.7333H3.33334"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ArrowArrange;
