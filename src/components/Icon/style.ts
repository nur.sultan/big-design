import styled from 'styled-components';

export const Wrapper = styled.span`
  line-height: 1;
  display: inline-block;
  vertical-align: -0.125em;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;

  svg {
    /* stroke: #ffffff;
        fill: #ffffff; */
  }
`;
