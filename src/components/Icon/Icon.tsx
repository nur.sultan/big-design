import React from 'react';

import * as Icons from '../../assets/icons';
import { IconProps } from './Icon.types';
import { Wrapper } from './style';

const Icon: React.FC<IconProps> = ({ strokeColor }) => {
  const icons = Object.values(Icons);
  return (
    <Wrapper className="icon-container">
      {icons.map((IconComponent, idx) => (
        <IconComponent
          fill="#fff"
          key={idx}
          stroke={strokeColor}
        />
      ))}
    </Wrapper>
  );
};

export default Icon;
