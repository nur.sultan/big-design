export interface IconProps {
  icon: string;
  strokeColor?: string;
}
