import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import Icon from './Icon';

export default {
  title: 'Design System/Icon',
  component: Icon,
} as ComponentMeta<typeof Icon>;

const BaseTemplate: ComponentStory<typeof Icon> = (args) => (
  <>
    <Icon
      {...args}
      strokeColor="#1369BF"
    />
  </>
);

export const Base = BaseTemplate.bind({});
Base.args = {};
