import React, { ReactNode } from 'react';

export interface ButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
  children: ReactNode;
  /** Variant of the button for visual styling */
  variant?: 'primary' | 'secondary' | 'outline' | 'warning' | 'text';
  /** Size of the button */
  size?: 'lg' | 'md' | 'sm';
  /** Disabled state of button */
  disabled?: boolean;
  loading?: boolean;
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
  fullWidth?: boolean;
}
