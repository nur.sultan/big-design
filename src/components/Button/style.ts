import { themeGet } from '@styled-system/theme-get';
import styled from 'styled-components';
import {
  buttonStyle,
  color,
  flexbox,
  fontSize,
  space,
  variant,
  width,
  WidthProps,
} from 'styled-system';

import { ButtonProps } from './Button.types';

type Props = ButtonProps & WidthProps;

const buttonSizes = variant({
  prop: 'size',
  variants: {
    sm: {
      padding: '4px 8px',
    },
    md: {
      padding: '8px 16px',
    },
    lg: {
      padding: '14px 28px',
    },
  },
});

export const ButtonContainer = styled.button.attrs(({ fullWidth, width }: Props) => {
  return {
    width: fullWidth ? [1] : width,
  };
})`
  border: 1px solid transparent;
  outline: 0;
  border-radius: 8px;
  line-height: 1.5;
  font-size: 16px;
  cursor: pointer;
  margin: 5px;
  display: flex;
  align-items: center;

  &:disabled {
    cursor: no-drop;
    border: 1px solid #d8d8d8;
    color: ${themeGet('colors.muted')};
    background-color: #e5e5e5;
  }

  ${color}
  ${space}
    ${fontSize}
    ${buttonStyle}
    ${width}
    ${buttonSizes}
    ${flexbox}
`;

export const Icon = styled.span`
  display: inherit;
`;
