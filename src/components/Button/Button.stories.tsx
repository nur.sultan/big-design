import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import BackIcon from '../../assets/icons/BackIcon';
import FlexBox from '../FlexBox';
import Button from './Button';

export default {
  title: 'Design System/Button',
  component: Button,
  argTypes: {},
  // eslint-disable-next-line prettier/prettier
} as ComponentMeta<typeof Button>;

const BaseTemplate: ComponentStory<typeof Button> = (args) => <Button {...args}>Default</Button>;

export const Base = BaseTemplate.bind({});
Base.args = {};

const VariantsTemplate: ComponentStory<typeof Button> = () => (
  <FlexBox alignItems="center">
    <Button variant="primary">Отправить</Button>
    <Button
      endIcon={<BackIcon stroke="#1369BF" />}
      startIcon={<BackIcon stroke="#1369BF" />}
      variant="secondary"
    >
      Secondary
    </Button>
    <Button variant="outline">Outline</Button>
    <Button
      disabled
      variant="primary"
    >
      Disabled
    </Button>
    <Button variant="warning">Warning</Button>
    <Button variant="text">Button</Button>
  </FlexBox>
);

export const Variants = VariantsTemplate.bind({});
Variants.args = {};

const VariantsLgTemplate: ComponentStory<typeof Button> = () => (
  <FlexBox alignItems={'center'}>
    <Button
      size="lg"
      variant="primary"
    >
      Отправить
    </Button>
    <Button
      size="lg"
      variant="secondary"
    >
      <BackIcon stroke="#1369BF" />
      Secondary
    </Button>
    <Button
      size="lg"
      variant="outline"
    >
      Outline
    </Button>
    <Button
      disabled
      size="lg"
    >
      Disabled
    </Button>
    <Button
      size="lg"
      variant="warning"
    >
      Warning
    </Button>
    <Button
      size="lg"
      variant="text"
    >
      Button
    </Button>
  </FlexBox>
);

export const VariantsLg = VariantsLgTemplate.bind({});
VariantsLg.args = {};
