import React from 'react';

import { ButtonProps } from './Button.types';
import { ButtonContainer, Icon } from './style';

const Button: React.FC<ButtonProps> = ({ children, startIcon, endIcon, ...rest }) => {
  return (
    <ButtonContainer {...rest}>
      {startIcon && <Icon>{startIcon}</Icon>}
      {children}
      {endIcon && <Icon>{endIcon}</Icon>}
    </ButtonContainer>
  );
};

Button.defaultProps = {
  size: 'md',
  fullWidth: false,
  variant: 'outline',
};

export default Button;
