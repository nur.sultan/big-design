import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import Radio from './Radio';

export default {
  title: 'Design System/Radio',
  component: Radio,
  argTypes: {},
} as ComponentMeta<typeof Radio>;

const BaseTemplate: ComponentStory<typeof Radio> = (args) => (
  <>
    <Radio {...args}>Radio</Radio>
    <Radio
      {...args}
      disabled
    >
      Radio Disabled
    </Radio>
    <Radio
      {...args}
      checked
      disabled
    >
      Radio Checked Disabled
    </Radio>
  </>
);

export const Base = BaseTemplate.bind({});
Base.args = {};
