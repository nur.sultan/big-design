import styled from 'styled-components';

import { RadioProps } from './Radio.types';

export const RadioWrapper = styled.div<RadioProps>`
  display: flex;
  align-items: center;
  position: relative;
  vertical-align: middle;

  input {
    position: relative;
    width: 20px;
    height: 20px;
    appearance: none;
    border: 1px solid #c2c2c2;
    border-radius: 100%;
    cursor: pointer;

    :checked[type='radio'] {
      background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='2.2' fill='%231369bf'/%3e%3c/svg%3e");
      background-repeat: no-repeat;
    }

    &:disabled {
      cursor: no-drop;
      background-color: #f5f5f5;
      border-color: #e0e0e0;

      & + label {
        color: #9e9e9e;
      }

      &:checked {
        :checked[type='radio'] {
          background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='2' fill='%23e0e0e0'/%3e%3c/svg%3e");
        }
      }
    }

    &:checked:enabled {
      background-color: #fff;
      border-color: #1369bf;
    }

    &:hover:enabled {
      border-color: #1369bf;
    }

    &:focus:enabled {
      outline: 3px solid#89b4df;
    }
  }

  label {
    margin-left: 4px;
    font-weight: 400;
    font-size: 14px;
  }
`;
