import React from 'react';

import { RadioProps } from './Radio.types';
import { RadioWrapper } from './style';

const Radio: React.FC<RadioProps> = ({ children, ...rest }) => {
  return (
    <RadioWrapper>
      <input
        type="radio"
        {...rest}
      />
      <label>{children}</label>
    </RadioWrapper>
  );
};

Radio.defaultProps = {
  disabled: false,
};

export default Radio;
