import React from 'react';

export interface RadioProps extends React.HTMLAttributes<HTMLInputElement> {
  disabled?: boolean;
  checked?: boolean;
  onChange?: () => void;
}
