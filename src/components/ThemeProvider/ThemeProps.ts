export interface ThemeProps {
  colors?: Record<string, string | number>;
  spaces: number[];
  borderRadius: number;
}
