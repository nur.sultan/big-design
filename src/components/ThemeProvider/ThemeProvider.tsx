import React from 'react';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';

import defaultTheme from './theme';
import { ThemeProps } from './ThemeProps';

type ThemeProviderProps = {
  theme?: ThemeProps;
  children?: React.ReactNode;
};

export const ThemeProvider: React.FC<ThemeProviderProps> = ({ children, theme = defaultTheme }) => {
  return <StyledThemeProvider theme={theme}>{children}</StyledThemeProvider>;
};
