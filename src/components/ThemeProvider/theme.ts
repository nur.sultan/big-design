import { DefaultTheme } from 'styled-components';

const animation = {
  default: '400ms ease-in',
  fast: '300ms ease-in',
};

const breakpoints = [
  // mobile
  '320px',
  // tablet
  '768px',
  // computer
  '992px',
  // desktop
  '1200px',
  // widescreen
  '1920px',
];

const colors = {
  text: '#111212',
  background: '#000',
  primary: '#1369BF',
  secondary: '#EEF4FA',
  muted: '#989898',
  gray: '#D3D7DA',
  highlight: 'hsla(205, 100%, 40%, 0.125)',
  white: '#FFF',
  black: '#111212',
  warning: '#BA1B1B',
};

const gradients = {
  subtle: `linear-gradient(180deg, ${colors.primary} 0%, ${colors.secondary} 100%)`,
  purple: `linear-gradient(180deg, ${colors.primary} 0%, #A000C4 100%)`,
  blue: `linear-gradient(180deg, #00D2FF 0%, ${colors.secondary} 100%)`,
};

const fonts = {
  body: 'Roboto, Helvetiva Neue, Helvetica, Aria, sans-serif',
  heading: 'Poppins, Helvetiva Neue, Helvetica, Aria, sans-serif',
  monospace: 'Menlo, monospace',
};

const defaultTheme: DefaultTheme = {
  animation,
  breakpoints,
  mediaQueries: {
    mobile: `@media screen and (min-width: ${breakpoints[0]})`,
    tablet: `@media screen and (min-width: ${breakpoints[1]})`,
    computer: `@media screen and (min-width: ${breakpoints[2]})`,
    desktop: `@media screen and (min-width: ${breakpoints[3]})`,
    widescreen: `@media screen and (min-width: ${breakpoints[4]})`,
  },
  colors,
  gradients,
  sizes: {
    avatar: 48,
  },
  fonts,
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96],
  buttons: {
    primary: {
      backgroundColor: `${colors.primary}`,
      color: '#fff',
      '&:hover:enabled': {
        backgroundColor: '#10579F',
        outline: '3px solid #D0E1F2',
      },
    },
    outline: {
      color: 'black',
      backgroundColor: 'transparent',
      border: '1px solid #E0E5ED',
      '&:hover:enabled': {
        backgroundColor: '#ECECEC',
      },
    },
    secondary: {
      color: '#1369BF',
      backgroundColor: `${colors.secondary}`,
      '&:hover:enabled': {
        outline: '3px solid #D0E1F2',
      },
    },
    warning: {
      color: '#fff',
      backgroundColor: `${colors.warning}`,
      '&:hover:enabled': {
        outline: '3px solid rgba(186, 27, 27, 0.2)',
      },
    },
    text: {
      color: '#1369BF',
      backgroundColor: 'transparent',
      '&:hover:enabled': {
        outline: '3px solid #D0E1F2',
      },
    },
  },
};

export default defaultTheme;
