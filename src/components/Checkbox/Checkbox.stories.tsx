import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import Checkbox from './Checkbox';

export default {
  title: 'Design System/Checkbox',
  component: Checkbox,
  argTypes: {},
} as ComponentMeta<typeof Checkbox>;

const BaseTemplate: ComponentStory<typeof Checkbox> = (args) => (
  <>
    <Checkbox {...args}>Checkbox</Checkbox>
    <Checkbox disabled>Checkbox Disabled</Checkbox>
    <Checkbox
      checked
      disabled
    >
      Checkbox checked Disabled
    </Checkbox>
  </>
);

export const Base = BaseTemplate.bind({});
Base.args = {};
