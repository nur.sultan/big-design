import styled from 'styled-components';

export const CheckBoxWrapper = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  vertical-align: middle;

  input {
    position: relative;
    width: 20px;
    height: 20px;
    appearance: none;
    border: 1px solid #c2c2c2;
    border-radius: 4px;
    cursor: pointer;

    :checked[type='checkbox'] {
      background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTUiIGhlaWdodD0iMTIiIHZpZXdCb3g9IjAgMCAxNSAxMiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4NCjxwYXRoIGQ9Ik01LjYxMTE3IDEwLjg2MTFMMTQuMzYxMSAyLjExMTVMMTQuMzYxMiAyLjExMTQ2QzE0LjQ4OTkgMS45ODI0OSAxNC41NjIyIDEuODA3NjYgMTQuNTYyMSAxLjYyNTQxQzE0LjU2MjEgMS40NDMxNiAxNC40ODk2IDEuMjY4MzkgMTQuMzYwOCAxLjEzOTUzQzE0LjIzMTkgMS4wMTA2NiAxNC4wNTcxIDAuOTM4MjM4IDEzLjg3NDkgMC45MzgxNzlDMTMuNjkyNiAwLjkzODEyIDEzLjUxNzggMS4wMTA0MyAxMy4zODg4IDEuMTM5MjFMMTMuMzg4OCAxLjEzOTI0TDUuMTI0OTYgOS40MDI3N0wxLjIzNjA0IDUuNTE0MkwxLjE5MjUzIDUuNTU3NzJMMS4yMzYwNCA1LjUxNDJDMS4xNzIyIDUuNDUwMzcgMS4wOTY0IDUuMzk5NzMgMS4wMTI5OSA1LjM2NTE5QzAuOTI5NTc5IDUuMzMwNjUgMC44NDAxOCA1LjMxMjg3IDAuNzQ5ODk3IDUuMzEyODhDMC42NTk2MTUgNS4zMTI4OSAwLjU3MDIxOSA1LjMzMDY4IDAuNDg2ODEyIDUuMzY1MjRDMC40MDM0MDUgNS4zOTk3OSAwLjMyNzYyMSA1LjQ1MDQ0IDAuMjYzNzg4IDUuNTE0MjhDMC4xOTk5NTQgNS41NzgxMyAwLjE0OTMyMSA1LjY1MzkyIDAuMTE0Nzc5IDUuNzM3MzNDMC4wODAyMzY1IDUuODIwNzUgMC4wNjI0NjE3IDUuOTEwMTUgMC4wNjI0Njk1IDYuMDAwNDNDMC4wNjI0NzczIDYuMDkwNzEgMC4wODAyNjczIDYuMTgwMTEgMC4xMTQ4MjQgNi4yNjM1MUMwLjE0OTM4MSA2LjM0NjkyIDAuMjAwMDI3IDYuNDIyNyAwLjI2Mzg3IDYuNDg2NTRMNC42Mzg4MSAxMC44NjExTDUuNjExMTcgMTAuODYxMVoiIGZpbGw9IndoaXRlIiBzdHJva2U9IndoaXRlIiBzdHJva2Utd2lkdGg9IjAuMTI1Ii8+DQo8L3N2Zz4NCg==);
      background-position: center;
      background-size: 14px;
      background-repeat: no-repeat;
    }

    &:disabled {
      cursor: no-drop;
      background-color: #e0e0e0;
      border-color: #e0e0e0;

      & + label {
        color: #9e9e9e;
      }
    }

    &:checked:enabled {
      background-color: #1369bf;
      border-color: #1369bf;
    }

    &:hover:enabled {
      border-color: #1369bf;

      &:not(:checked) {
        &:after {
          content: '';
          width: 10px;
          height: 10px;
          background-color: #1369bf;
          position: absolute;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          margin: auto;
        }
      }
    }

    &:focus {
      outline: 3px solid#89b4df;
    }
  }

  label {
    margin-left: 4px;
    font-weight: 400;
    font-size: 14px;
  }
`;
