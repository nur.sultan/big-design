import React from 'react';

import { CheckboxProps } from './Checkbox.types';
import { CheckBoxWrapper } from './style';

const Checkbox: React.FC<CheckboxProps> = ({ children, ...rest }) => {
  return (
    <CheckBoxWrapper>
      <input
        type="checkbox"
        {...rest}
      />
      <label>{children}</label>
    </CheckBoxWrapper>
  );
};

Checkbox.defaultProps = {
  disabled: false,
};

export default Checkbox;
