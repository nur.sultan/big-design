import * as React from 'react';

function SvgCheckboxCheckIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      fill="none"
      height="1em"
      viewBox="0 0 15 12"
      width="1em"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M5.611 10.861l8.75-8.75h0a.688.688 0 00-.972-.972h0L5.125 9.403 1.236 5.514l-.043.044.043-.044a.687.687 0 10-.972.973l4.375 4.374h.972z"
        fill="#fff"
        stroke="#fff"
        strokeWidth={0.125}
      />
    </svg>
  );
}

const MemoSvgCheckboxCheckIcon = React.memo(SvgCheckboxCheckIcon);
export default MemoSvgCheckboxCheckIcon;
