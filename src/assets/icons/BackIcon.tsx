import * as React from 'react';

function SvgBackIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      height="1em"
      viewBox="0 0 16 16"
      width="1em"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path d="M3.333 8h9.334M6.667 4.667L3.333 8M6.667 11.333L3.333 8" />
    </svg>
  );
}

const MemoSvgBackIcon = React.memo(SvgBackIcon);
export default MemoSvgBackIcon;
