import * as React from 'react';

function SvgArrowArrangeIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      height="1em"
      width="1em"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path d="M10.735 7.195l1.932-1.93-1.932-1.932M3.333 5.267h9.334M5.265 8.805l-1.932 1.93 1.932 1.932M12.667 10.733H3.333" />
    </svg>
  );
}

const MemoSvgArrowArrangeIcon = React.memo(SvgArrowArrangeIcon);
export default MemoSvgArrowArrangeIcon;
