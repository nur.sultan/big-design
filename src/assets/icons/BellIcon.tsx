import * as React from 'react';

function SvgBellIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      height="1em"
      width="1em"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path d="M2.228 11.02c.377-.647.822-1.867.822-4.02v-.5A4.95 4.95 0 018 1.55h.038c2.708.02 4.912 2.266 4.912 5.007V7c0 2.072.412 3.28.78 3.946h-.002l.044.075a.95.95 0 01-.82 1.429H3.048a.95.95 0 01-.82-1.43zM9.996 13.5h-4a.5.5 0 000 1h4a.5.5 0 000-1z" />
    </svg>
  );
}

const MemoSvgBellIcon = React.memo(SvgBellIcon);
export default MemoSvgBellIcon;
