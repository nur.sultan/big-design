
import {ThemeProvider} from "../src/components/ThemeProvider/ThemeProvider";
// import "../src/scss/global.scss"
// import "../src/fonts.scss"
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  previewTabs: {
    canvas: {
      hidden: true,
    },
  },
  controls: {
    // matchers: {
    //   color: /(background|color)$/i,
    //   date: /Date$/,
    // },
  },
}

export const decorators = [
  (Story) => (
    <ThemeProvider>
      <Story />
    </ThemeProvider>
  ),
];